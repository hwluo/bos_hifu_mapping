#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 10 12:32:37 2019

@author: huiwenluo
"""
import scipy.io as sio 
import numpy as np
import h5py
import os
import matplotlib.pyplot as plt

#%%
Basepath = '/Users/huiwenluo/Library/Mobile Documents/com~apple~CloudDocs/Desktop/bos_hifu_mapping/codeanddata/CompareHydrophone'
dict1 = sio.loadmat(Basepath+'/Hydro_116.mat')
maph = dict1['HydroRMS']
mapz = dict1['BOSRMS']
import matplotlib.gridspec as gridspec
maph = maph[:,3:-4]
mapz = mapz[:,4:-3]
csfont ={'family': 'Times New Roman',
        'weight': 'normal',
        'size': 18,
        'fontname':'Times New Roman',
        }
nx = maph.shape[0]
nz = maph.shape[1]
ds = 1.7/6
fig, axs = plt.subplots(1, 2)
plt.subplot(121)
plt.imshow(maph.T,cmap ='inferno',vmin = 0,vmax = 6500,extent = [-nx*ds/2,nx*ds/2,-nz*ds/2,nz*ds/2])
plt.xlabel('x(mm)',**csfont)
plt.ylabel('z(mm)',**csfont)
plt.xticks(**csfont)
plt.yticks(**csfont)
plt.subplot(122)
plt.imshow(mapz.T,cmap ='inferno',vmin =0,vmax = 6500,extent = [-nx*ds/2,nx*ds/2,-nz*ds/2,nz*ds/2])
plt.xlabel('x(mm)',**csfont)
plt.xticks(**csfont)
plt.yticks([])
cax = plt.axes([ 0.91, 0.11,0.03, 0.77])

cbar = plt.colorbar(orientation='vertical',cax = cax,ax = axs)
cbar.ax.tick_params(labelsize=16) 
#%%
Basepath = '/Users/huiwenluo/Library/Mobile Documents/com~apple~CloudDocs/Desktop/bos_hifu_mapping/codeanddata/CompareHydrophone'
dict1 = sio.loadmat(Basepath+'/Hydro_225.mat')
maph = dict1['HydroRMS']
mapz = dict1['BOSRMS']
import matplotlib.gridspec as gridspec
maph = maph[:,3:-4]
mapz = mapz[:,4:-3]
csfont ={'family': 'Times New Roman',
        'weight': 'normal',
        'size': 18,
        'fontname':'Times New Roman',
        }
nx = maph.shape[0]
nz = maph.shape[1]
ds = 1.7/6
fig, axs = plt.subplots(1, 2)
plt.subplot(121)
plt.imshow(maph.T,cmap ='inferno',vmin = 0,vmax = 3000,extent = [-nx*ds/2,nx*ds/2,-nz*ds/2,nz*ds/2])
plt.xlabel('x(mm)',**csfont)
plt.ylabel('z(mm)',**csfont)
plt.xticks(**csfont)
plt.yticks(**csfont)
plt.subplot(122)
plt.imshow(mapz.T,cmap ='inferno',vmin =0,vmax = 3000,extent = [-nx*ds/2,nx*ds/2,-nz*ds/2,nz*ds/2])
plt.xlabel('x(mm)',**csfont)
plt.xticks(**csfont)
plt.yticks([])
cax = plt.axes([ 0.91, 0.11,0.03, 0.77])

cbar = plt.colorbar(orientation='vertical',cax = cax,ax = axs)
cbar.ax.tick_params(labelsize=16) 