Basepath = '/Users/huiwenluo/Desktop/vandy/research/fullwavemodel/pressuredata/forwardmodel4_data/hydrophone_225';
load([Basepath,'/data100mv_1.mat']);
xp = xp*1e-3;
zp = zp*1e-3;
for ii = 2:5
    data = load(['/Users/huiwenluo/Desktop/vandy/research/fullwavemodel/pressuredata/forwardmodel4_data/hydrophone_225/data100mv_',num2str(ii),'.mat']);
    volts = cat(1,volts,data.volts);
end
p = -volts/0.11805*1e6;
maxp = squeeze(max(p,[],4));
[fx,fy] = find(max(maxp,[],3) == max(maxp(:)));
[fz,ft] = find(squeeze(p(fx,fy,:,:)) == max(maxp(:)));
projnew = squeeze(sum(p(:,:,:,:)*mean(diff(yp))*1e-3,2));
rmsproj = [];
for ii = 1:size(projnew,1)
    for jj = 1:size(projnew,2)
        [~,locs] = findpeaks(squeeze(projnew(ii,jj,:)),'MinPeakHeight',max(projnew(ii,jj,:))*0.6);
        rmsproj(ii,jj) = sqrt(mean(projnew(ii,jj,round(mean(locs(:)))-100:round(mean(locs(:)))+100).^2));
    end
end
%%
Basepath ='/Users/huiwenluo/Desktop/vandy/research/fullwavemodel/pressuredata/forwardmodel4_data/newtilehis20190422_225';
rep = 5;
rmsblur_all = [];
for jj = 1:rep
    load([Basepath,'/100mv/','predict100mv',num2str(jj),'_1009']);
    rmsblur_all(:,:,jj) = rmsblur;
    %  rmsunblur_all(:,:,jj) = rmsunblur;
end
rmsblur2 = mean(rmsblur_all,3);
rmsblur2 = rmsblur2(:,1:200);
ds = 0.0017/6;
xpp2 = [flip(xp(fx)-ds:-ds:xp(1)),xp(fx):ds:xp(end)];
zpp2 = [flip(zp(fz)-ds:-ds:zp(1)),zp(fz):ds:zp(end)];
HydroRMS = interp1(zp,interp1(xp,rmsproj,xpp2,'linear')',zpp2,'linear');
HydroRMS = HydroRMS(:,20:130);

dxx = size(rmsblur2,1)-size(HydroRMS,1);
dzz = size(rmsblur2,2)-size(HydroRMS,2);
rmse = [];
for ii = 1:dxx
    for jj = 1:dzz
        tmprmsblur = rmsblur2(ii:ii+size(HydroRMS,1)-1,jj:jj+size(HydroRMS,2)-1);
        tmpdiff = HydroRMS/max(HydroRMS(:))-tmprmsblur/max(tmprmsblur(:));
        rmse(ii,jj) = sqrt(mean(tmpdiff(:).^2));
    end
end
[mm,nn] = find(rmse==min(rmse(:)));

BOSRMS = rmsblur2(mm:mm+size(HydroRMS,1)-1,nn:nn+size(HydroRMS,2)-1);
figure;
subplot(131); imagesc(BOSRMS',[0 3000]); colorbar
subplot(132); imagesc(HydroRMS',[0 3000]); colorbar
subplot(133); imagesc(abs(HydroRMS'-BOSRMS')); colorbar
save('Hydro_225','BOSRMS','HydroRMS');
