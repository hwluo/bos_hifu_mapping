
%% 200mv map
Basepath = '/Users/huiwenluo/Desktop/vandy/research/fullwavemodel/pressuredata/forwardmodel4_data';
load([Basepath,'/data0326_200mv_1.mat']);
for ii = 2:6
    data = load(['data0326_200mv_',num2str(ii),'.mat']);
    volts = cat(1,volts,data.volts);
end
xp = xp*1e-3;
zp = zp*1e-3;
p = -volts/0.16227*1e6;%0.09681*1e6;
maxp = max(p,[],4);
[fx,fy] = find(max(maxp,[],3) == max(maxp(:)));
[fz,ft] = find(squeeze(p(fx,fy,:,:)) == max(maxp(:)));
sig_focus = squeeze(p(fx,fy,fz,:));
%%
projnew = squeeze(sum(p(:,3:end-5,:,:)*mean(diff(yp))*1e-3,2));
dxp = mean(diff(xp));
rmsproj = [];
for ii = 1:size(projnew,1)
    for jj = 1:size(projnew,2)
        [~,locs] = findpeaks(squeeze(projnew(ii,jj,:)),'MinPeakHeight',max(projnew(ii,jj,:))*0.6);
        rmsproj(ii,jj) = sqrt(mean(projnew(ii,jj,round(mean(locs(:)))-50:round(mean(locs(:)))+50).^2));
    end
end
Basepath ='/Users/huiwenluo/Desktop/vandy/research/fullwavemodel/pressuredata/forwardmodel4_data/newtilehis20190410_v2';
rep = 5;
rmsblur_all = [];
rmsunblur_all = [];
for jj = 1:rep
    load([Basepath,'/200mv/','predict200mv',num2str(jj),'_1009.mat']);
    rmsblur_all(:,:,jj) = rmsblur;
end
ds = 0.0017/6;
xpp = [flip(xp(fx)-ds:-ds:xp(1)),xp(fx):ds:xp(end)];
zpp = [flip(zp(fz)-ds:-ds:zp(1)),zp(fz):ds:zp(end)];
HydroRMS = interp1(zp,interp1(xp,rmsproj,xpp,'linear')',zpp,'linear');
BOSRMS = mean(rmsblur_all(:,1:end,:),3);
HydroRMS = HydroRMS(3:end-3,4:end-4);
dxx = size(BOSRMS,1)-size(HydroRMS,1);
dzz = size(BOSRMS,2)-size(HydroRMS,2);
rmse = [];
for ii = 1:dxx
    for jj = 1:dzz
        tmprmsblur = BOSRMS(ii:ii+size(HydroRMS,1)-1,jj:jj+size(HydroRMS,2)-1);
        tmpdiff = tmprmsblur/max(tmprmsblur(:))-HydroRMS/max(HydroRMS(:));
        rmse(ii,jj) = sqrt(sum(tmpdiff(:).^2));
    end
end
[mm,nn] = find(rmse==min(rmse(:)));
BOSRMS = BOSRMS(mm:mm+size(HydroRMS,1)-1,nn:nn+size(HydroRMS,2)-1);
imagesc(cat(1,BOSRMS,HydroRMS,BOSRMS-HydroRMS))
 