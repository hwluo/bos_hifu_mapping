#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 10 12:49:49 2019

@author: huiwenluo
"""

#%%
import scipy.io as sio 
import numpy as np
import h5py
import os
import matplotlib.pyplot as plt
Basepath2 = '/Users/huiwenluo/Library/Mobile Documents/com~apple~CloudDocs/Desktop/bos_hifu_mapping/codeanddata/Aberration'
dict2 = sio.loadmat(Basepath2+'/BlockLeft.mat')

maph = dict2['HydroRMS']
mapz = dict2['BOSRMS']
import matplotlib.gridspec as gridspec
ds = 1.7/6
csfont ={'family': 'Times New Roman',
        'weight': 'normal',
        'size': 18,
        'fontname':'Times New Roman',
        }
nx = maph.shape[0]
nz = maph.shape[1]
fig, axs = plt.subplots(1, 2)
plt.subplot(121)
plt.imshow(maph.T,cmap ='inferno',vmin = 0,vmax = 2500,extent = [-nx*ds/2,nx*ds/2,-nz*ds/2,nz*ds/2])
plt.xlabel('x(mm)',**csfont)
plt.ylabel('z(mm)',**csfont)
#plt.title('Hydrophone-measured projected pressure',**csfont)
plt.xticks(**csfont)
plt.yticks(**csfont)
plt.subplot(122)
plt.imshow(mapz.T,cmap ='inferno',vmin = 0,vmax = 2500,extent = [-nx*ds/2,nx*ds/2,-nz*ds/2,nz*ds/2])
plt.xlabel('x(mm)',**csfont)
#plt.title('Reconstructed projected pressure',**csfont)
plt.yticks([])
plt.xticks(**csfont)
plt.yticks(**csfont)
plt.tight_layout()

cax = plt.axes([ 0.88, 0.155,0.03, 0.81])
cbar = plt.colorbar(orientation='vertical',cax = cax,ax = axs,ticks = [0,500,1000,1500,2000,2500])
cbar.ax.tick_params(labelsize=16) 
#%%
dict2 = sio.loadmat(Basepath2+'/BlockBottom.mat')
ds = 1.7/6;
maph = dict2['HydroRMS']
mapz = dict2['BOSRMS']
maph = maph[:-8,10:]
mapz = mapz[:-8,10:]
from matplotlib import rcParams
rcParams['font.family'] = 'Times New Roman'

csfont ={'family': 'Times New Roman',
        'weight': 'normal',
        'size': 18,
        'fontname':'Times New Roman',
        }
nx = maph.shape[0]
nz = maph.shape[1]
fig, axs = plt.subplots(1, 2)
plt.subplot(121)
plt.imshow(maph.T,cmap ='inferno',vmin =0,vmax =2500,extent = [-nx*ds/2,nx*ds/2,-nz*ds/2,nz*ds/2])

plt.xlabel('x(mm)',**csfont)
plt.ylabel('z(mm)',**csfont)
#plt.title('Hydrophone-measured projected pressure',**csfont)
plt.xticks(**csfont)
plt.yticks(**csfont)
plt.subplot(122)
plt.imshow(mapz.T,cmap ='inferno',vmin =0,vmax = 2500,extent = [-nx*ds/2,nx*ds/2,-nz*ds/2,nz*ds/2])
plt.xlabel('x(mm)',**csfont)
#plt.title('Reconstructed projected pressure',**csfont)
plt.yticks([])
plt.xticks(**csfont)
plt.yticks(**csfont)
plt.tight_layout()

cax = plt.axes([ 0.88, 0.155,0.03, 0.81])
cbar = plt.colorbar(orientation='vertical',spacing = 'proportional',cax = cax,ax = axs)
cbar.ax.tick_params(labelsize=16) 