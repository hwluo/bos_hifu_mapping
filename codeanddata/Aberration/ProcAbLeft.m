load('/Users/huiwenluo/Desktop/vandy/research/fullwavemodel/pressuredata/forwardmodel4_data/20190728hydro/data200mv_1b2.mat')
for ii = 2:4
    data = load(['/Users/huiwenluo/Desktop/vandy/research/fullwavemodel/pressuredata/forwardmodel4_data/20190728hydro/data200mv_',num2str(ii),'b2.mat'],'volts');
    volts = cat(1,volts,data.volts);
end
volt = -volts/0.10441*1e6;
[fx2,fy2] = find(max(max(volt,[],4),[],3) == max(volt(:)));
[fz2,ft2] = find(squeeze(max(max(volt,[],1),[],2)) == max(volt(:)));
xp = xp*1e-3;
zp = zp*1e-3;
projnew = squeeze(sum(volt*mean(diff(yp))*1e-3,2));
rmsproj = [];
for ii = 1:size(projnew,1)
    for jj = 1:size(projnew,2)
        [~,locs] = findpeaks(squeeze(projnew(ii,jj,:)),'MinPeakHeight',max(projnew(ii,jj,:))*0.6);
        rmsproj(ii,jj) = sqrt(mean(projnew(ii,jj,round(mean(locs(:)))-100:round(mean(locs(:)))+100).^2));
    end
end
rmsblur = [];
for ii = 1:5
    data = load(['/Users/huiwenluo/Desktop/vandy/research/fullwavemodel/pressuredata/forwardmodel4_data/newtilehis20190728_23/150mv/predict150mv',num2str(ii),'_1009.mat']);
    rmsblur(:,:,ii) = data.rmsblur;
end
ds = 0.0017/6;
rmsblur2 = mean(rmsblur,3);
xpp2 = [flip(xp(fx2)-ds:-ds:xp(1)),xp(fx2):ds:xp(end)];
zpp2 = [flip(zp(fz2)-ds:-ds:zp(1)),zp(fz2):ds:zp(end)];
HydroRMS = interp1(zp,interp1(xp,rmsproj,xpp2,'linear')',zpp2,'linear');
%interprms2 = interprms2(:,30:end);
HydroRMS = HydroRMS(15+5:51-5,80:end-50);

dxx = size(rmsblur2,1)-size(HydroRMS,1);
dzz = size(rmsblur2,2)-size(HydroRMS,2);
rmse = [];
for ii = 1:dxx
    for jj = 1:dzz
        tmprmsblur = rmsblur2(ii:ii+size(HydroRMS,1)-1,jj:jj+size(HydroRMS,2)-1);
        tmpdiff = (tmprmsblur-mean(tmprmsblur(:)))/std(tmprmsblur(:))-(HydroRMS-mean(HydroRMS(:)))/std(HydroRMS(:));
        rmse(ii,jj) = sqrt(mean(tmpdiff(:).^2));
    end
end
[mm,nn] = find(rmse==min(rmse(:)));
nn = nn - 3 ;
BOSRMS = rmsblur2(mm:mm+size(HydroRMS,1)-1,nn:nn+size(HydroRMS,2)-1);
figure;
subplot(131); imagesc(BOSRMS',[0 2500]); colorbar
subplot(132); imagesc(HydroRMS',[0 2500]); colorbar
subplot(133); imagesc(abs(HydroRMS'-BOSRMS')); colorbar
save('BlockLeft','BOSRMS','HydroRMS')