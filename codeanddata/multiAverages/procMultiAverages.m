%%

vv = [50:50:200];
basepath = '/Users/huiwenluo/Desktop/vandy/research/fullwavemodel/pressuredata/forwardmodel4_data/newtilehis20191005';
rmsblurall = [];
rmsunblurall = [];
for ii = 1:length(vv)
    for jj = 1:10
        load([basepath,'/',num2str(vv(ii)),'mv/predict',num2str(vv(ii)),'mv',num2str(jj),'_1009.mat']);
        rmsblurall(:,:,ii,jj) = rmsblur;
        rmsunblurall(:,:,ii,jj) = rmsunblur;
    end
end
save('MultiAverages','rmsblurall','rmsunblurall');

rmsblurall= rmsblurall(24:43,26:95,:,:);
rmsunblurall = rmsunblurall(24:43,26:95,:,:);
rmse = [];
for ii = 1:9
    for jj = 1:length(vv)
        I0 =  mean(squeeze(rmsblurall(:,:,jj,1:ii)),3);
        I1 = mean(squeeze(rmsblurall(:,:,jj,1:ii+1)),3);
        mse(ii,jj)= immse(I1,I0);
    end
end
save('MultiAverages','mse','-append');
