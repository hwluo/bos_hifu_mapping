#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 10 12:56:38 2019

@author: huiwenluo
"""
import scipy.io as sio 
import numpy as np
import h5py
import os
import matplotlib.pyplot as plt
Basepath2 = '/Users/huiwenluo/Library/Mobile Documents/com~apple~CloudDocs/Desktop/bos_hifu_mapping/codeanddata/multiAverages'
dict2 = sio.loadmat(Basepath2+'/MultiAverages.mat')
#%%
rmsblurall = dict2['rmsblurall']
import matplotlib.gridspec as gridspec

csfont ={'family': 'Times New Roman',
        'weight': 'normal',
        'size': 15,
        'fontname':'Times New Roman',

        }
from matplotlib import rcParams
rcParams['font.family'] = 'Times New Roman'
rcParams['font.sans-serif'] = ['stixsans']
ds = 0.0017/6*1e3;
rmsblurall = rmsblurall[12:-8,15:-15,:,:]
nx = rmsblurall.shape[0]
nz = rmsblurall.shape[1]
fig, axs = plt.subplots(2,5)
for ii in range(10):
    plt.subplot(2,5,ii+1)
    plt.imshow(np.mean(rmsblurall[:,:,3,:(ii+1)],axis = 2).T,cmap ='inferno',vmin =0,vmax = 6000,extent = [-nx*ds/2,nx*ds/2,-nz*ds/2,nz*ds/2])
    plt.title('NSA: '+str(ii+1),**csfont)
    plt.xticks(**csfont)
    plt.yticks(**csfont)
    if (ii+1)%5 != 1:
        plt.yticks([])
    else:
        plt.ylabel('z(mm)',**csfont)
    if ii<5:
        plt.xticks([])
    else:
        plt.xlabel('x(mm)',**csfont)
cax = plt.axes([ 0.9, 0.11,0.03, 0.77])
cbar = plt.colorbar(orientation='vertical',cax = cax,ax = axs)
cbar.ax.tick_params(labelsize=13) 

#%%
csfont ={'family': 'Times New Roman',
        'weight': 'normal',
        'size': 18,
        'fontname':'Times New Roman',

        }
dict2 = sio.loadmat(Basepath2+'/MultiAverages.mat')
mse = dict2['mse'].T
plt.plot(np.arange(1,10,1),mse[0,:].T,linestyle = '-',marker = '.',linewidth = 2,label = '50mVpp')
plt.plot(np.arange(1,10,1),mse[1,:].T,linestyle = '-',marker = '.',linewidth = 2,label = '100mVpp')
plt.plot(np.arange(1,10,1),mse[2,:].T,linestyle = '-',marker = '.',linewidth = 2,label = '150mVpp')
plt.plot(np.arange(1,10,1),mse[3,:].T,linestyle = '-',marker = '.',linewidth = 2,label = '200mVpp')
plt.xticks([1,2,3,4,5,6,7,8,9],**csfont)
plt.xlabel('The numer of averages',**csfont)
plt.yticks(**csfont)
plt.title('Mean-squared-error of adjacent NSAs',**csfont)
plt.legend(loc = 'upper right')