#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 10 09:31:09 2019

@author: huiwenluo
"""
#%%
import scipy.io as sio 
import numpy as np
import os
import matplotlib.pyplot as plt
from matplotlib import rcParams
Basepath = '/Users/huiwenluo/Library/Mobile Documents/com~apple~CloudDocs/Desktop/bos_hifu_mapping/codeanddata/linearity/'
dict1 = sio.loadmat(Basepath+'Linearity.mat')
rmsblur = dict1['rmsblur_all']
rmsunblur = dict1['rmsunblur_all']
vv = np.arange(0,225,25)
mean_blur = rmsblur.mean(axis = 3)
mean_unblur = rmsunblur.mean(axis = 3)
nx = mean_blur.shape[0]
nz = mean_unblur.shape[1]
ds= 1.7/6
fig, axs = plt.subplots(3, 3)
csfont ={'family': 'Times New Roman',
        'weight': 'normal',
        'size': 15,
        'fontname':'Times New Roman',
        }
rcParams['font.family'] = 'Times New Roman'
rcParams['font.sans-serif'] = ['stixsans']
for ii in range(0,9):
    plt.subplot(330+1+ii)
    plt.imshow(mean_blur[13:nx-13,:,ii].T,cmap ='inferno',vmin = 0,vmax = 6500,extent = [-nx*ds/2,nx*ds/2,-nz*ds/2,nz*ds/2])
    plt.title(str(vv[ii])+'mv',**csfont)
    plt.xticks(**csfont)
    plt.yticks(**csfont)
    if (ii+1)%3 != 1:
        plt.yticks([])
    else:
        plt.ylabel('z(mm)',**csfont)
    if ii>5:
        plt.xlabel('x(mm)',**csfont)
    if ii<6:
        plt.xticks([])
cax = plt.axes([ 0.175, 0.925,0.68, 0.015])
cbar = plt.colorbar(orientation='horizontal',cax = cax,ax = axs,ticks=[0,2000,4000,6000])
cbar.ax.tick_params(labelsize=15) 
cbar.ax.xaxis.set_ticks_position('top')
#%%
meanFocus_bos = dict1['meanFocus_bos']
stdFocus_bos = dict1['stdFocus_bos']
focus_hydro = dict1['focus_hydro']
fit_hydro = dict1['fit_hydro']
csfont ={'family': 'Times New Roman',
        'weight': 'normal',
        'size': 15,
        'fontname':'Times New Roman',
        }
plt.subplot(212)
plt.errorbar(vv,meanFocus_bos[0,:],stdFocus_bos[0,:],ecolor = 'black',fmt = 'k',linewidth = 3,label = 'Reconstructed Pressure')
plt.plot(vv.T,fit_hydro[0,:],color = 'gray', linestyle = '--',linewidth = 3,label = 'Hydrohphone-measured Pressure')
plt.xticks([0,25,50,75,100,125,150,175,200],**csfont)
plt.xlabel('Voltage (mv)',**csfont)
plt.yticks(**csfont)
plt.title('RMS Projected Pressure (Pa$\cdot$m)',**csfont)
plt.legend(loc = 'lower right')


mm = 34; nn = 71
A_s = mean_blur[mm-1:mm+1,nn-3:nn+3,:].mean(axis =(0,1))
A_noise =mean_unblur[mm-1:mm+1,nn-3:nn+3,:].mean(axis = (0,1))
plt.subplot(211)
plt.plot(vv,(A_s/A_noise)**2,color = 'black',linewidth = 3)
plt.xticks([0,25,50,75,100,125,150,175,200],**csfont)
plt.xlabel('Voltage (mv)',**csfont)
plt.yticks(**csfont)
plt.title(r'SNR around the Focus',**csfont)
plt.legend(loc = 'lower right')

