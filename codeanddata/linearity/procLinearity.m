Basepath ='/Users/huiwenluo/Desktop/vandy/research/fullwavemodel/pressuredata/forwardmodel4_data/newtilehis20190410_v2';
vv = [0:25:200];
rep = 5;
rmsblur_all = [];
rmsunblur_all = [];
for ii = 1:length(vv)
    for jj = 1:rep
        load([Basepath,'/',num2str(vv(ii)),'mv/','predict',num2str(vv(ii)),'mv',num2str(jj),'_1009.mat']);
        rmsblur_all(:,:,ii,jj) = rmsblur;
        rmsunblur_all(:,:,ii,jj) = rmsunblur;
    end
end
mm = 34; nn =71;
meanFocus_bos = [];
stdFocus_bos = [];
for ii = 1:length(vv)
    meanFocus_bos(ii) = squeeze(mean(rmsblur_all(mm,nn,ii,:),4));
    stdFocus_bos(ii) = squeeze(std(rmsblur_all(mm,nn,ii,:),[],4));
end

Basepath = '/Users/huiwenluo/Desktop/vandy/research/fullwavemodel/pressuredata/hydrophone101_0429';
vv1 =[50:50:200];
focus_hydro = [];
for ii = 1:length(vv1)
    load([Basepath,'/data',num2str(vv1(ii)),'.mat']);
    p1 = -volts/0.09681*1e6;
    pProj1 = squeeze(sum(p1(:,:,:,2500:4500),2))*mean(diff(yp))*1e-3;
    rmsproj1 = sqrt(mean(pProj1.^2,2));
    focus_hydro = [focus_hydro,rmsproj1(3)];
end
ff1 = polyfit(vv1,focus_hydro(1:end),1);
fit_hydro = polyval(ff1,vv)-polyval(ff1,0);
save('Linearity','fit_hydro','vv','meanFocus_bos','stdFocus_bos','focus_hydro','rmsblur_all','rmsunblur_all');