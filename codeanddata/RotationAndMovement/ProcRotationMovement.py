#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 10 10:08:58 2019

@author: huiwenluo
"""
#%%
import scipy.io as sio 
import numpy as np
import h5py
import os
import matplotlib.pyplot as plt
Basepath = '/Users/huiwenluo/Desktop/vandy/research/fullwavemodel/pressuredata/forwardmodel4_data/newtilehis20190410_other/'
vvnum = os.listdir(Basepath)
rmsblur_all = []
for vv in range(len(vvnum)):
    mvolt = vvnum[vv]
    dict1 = sio.loadmat(Basepath+str(vvnum[vv])+'/predict'+str(vvnum[vv])+'1_1009.mat')
    nx = int(dict1['nx'])
    nz = int(dict1['nz'])
    blur = np.zeros((nx,nz,5))
    for pn in range(5):
        print(mvolt)
        dict1 = sio.loadmat(Basepath+str(vvnum[vv])+'/predict'+str(vvnum[vv])+str(pn+1)+'_1009.mat')
        rmsblur = dict1['rmsblur']
        blur[:,:,pn] = rmsblur
    blur[blur<0] = 0
    rmsblur_all.append(blur.mean(axis = 2))
ds= 0.0017/6*1e3
fig, axs = plt.subplots(1, 3)
from matplotlib import rcParams
rcParams['font.family'] = 'Times New Roman'
##rotation
nx = 64
nz = 100
csfont ={'family': 'Times New Roman',
        'weight': 'normal',
        'size': 18,
        'fontname':'Times New Roman',
        }
plt.subplot(131)
plt.imshow(rmsblur_all[1][3:4+nx,15:15+nz].T,cmap ='inferno',vmin = 0,vmax = 6500,extent = [-nx*ds/2,nx*ds/2,-nz*ds/2,nz*ds/2])
plt.ylabel('z(mm)',**csfont)
plt.title('0$^{\circ}$\n(Reference)',**csfont)
plt.xlabel('x(mm)',**csfont)
plt.xticks(**csfont)
plt.yticks(**csfont)
    
plt.subplot(132)
plt.imshow(rmsblur_all[6][0:nx,0:nz].T,cmap ='inferno',vmin = 0,vmax = 6500,extent = [-nx*ds/2,nx*ds/2,-nz*ds/2,nz*ds/2])
plt.title('15$^{\circ}$',**csfont)
plt.xlabel('x(mm)',**csfont)
plt.xticks(**csfont)
plt.yticks(**csfont)
plt.yticks([])
plt.subplot(133)
plt.imshow(rmsblur_all[5][0:nx,0:nz].T,cmap ='inferno',vmin = 0,vmax = 6500,extent = [-nx*ds/2,nx*ds/2,-nz*ds/2,nz*ds/2])
plt.xlabel('x(mm)',**csfont)
plt.xticks(**csfont)
plt.yticks(**csfont)
plt.title('30$^{\circ}$',**csfont)
plt.yticks([])
cax = plt.axes([ 0.915, 0.115,0.015, 0.75])
cbar = plt.colorbar(orientation='vertical',cax = cax,ax = axs,ticks=[0,2000,4000,6000])
cbar.ax.tick_params(labelsize=16) 
#%%
fig, axs = plt.subplots(1, 3)
plt.subplot(131)
plt.imshow(rmsblur_all[0][6+5:nx+6-5,11:nz+11].T,cmap ='inferno',vmin = 0,vmax = 5000,extent = [-nx*ds/2,nx*ds/2,-nz*ds/2,nz*ds/2])
plt.title('0cm \n (Reference)',**csfont)
plt.xlabel('x(mm)',**csfont)
plt.ylabel('z(mm)',**csfont)
plt.xticks(**csfont)
plt.yticks(**csfont)
plt.subplot(132)
plt.imshow(rmsblur_all[2][6+5:nx+6-5,11:nz+11].T,cmap ='inferno',vmin = 0,vmax = 5000,extent = [-nx*ds/2,nx*ds/2,-nz*ds/2,nz*ds/2])
plt.title('-2.5cm',**csfont)
plt.xlabel('x(mm)',**csfont)
plt.yticks([])
plt.xticks(**csfont)
plt.yticks(**csfont)
plt.subplot(133)
plt.imshow(rmsblur_all[4][5:nx-5,4:nz+4].T,cmap ='inferno',vmin = 0,vmax = 5000,extent = [-nx*ds/2,nx*ds/2,-nz*ds/2,nz*ds/2])
plt.title('+2.5cm',**csfont)
plt.xlabel('x(mm)',**csfont)
plt.yticks([])
plt.xticks(**csfont)
plt.yticks(**csfont)
    
cax = plt.axes([ 0.915, 0.115,0.015, 0.75])
cbar = plt.colorbar(orientation='vertical',cax = cax,ax = axs,ticks=[0,2500,5000])
cbar.ax.tick_params(labelsize=16) 